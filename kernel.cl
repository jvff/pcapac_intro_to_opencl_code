__kernel void matrixMultiplication(
        __global const float* A,
        __global const float* B,
        __global float* C,
        uint Bcols,
        uint N) {
    int i = get_global_id(0);
    int j = get_global_id(1);

    float result = 0.f;

    for (uint k = 0; k < N; ++k)
        result += A[i*N + k] * B[k*Bcols + j];

    C[i*Bcols + j] = result;
}
