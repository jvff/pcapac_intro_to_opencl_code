#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <CL/cl.h>

const char* loadFromFile(const char* fileName) {
    FILE* file = fopen(fileName, "r");

    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    rewind(file);

    char* fileContents = malloc(fileSize + 1);
    fread(fileContents, fileSize, 1, file);
    fileContents[fileSize] = '\0';

    return fileContents;
}

int main(int argc, char* argv[]) {
    cl_platform_id platforms[10];
    int numPlatforms;

    clGetPlatformIDs(10, platforms, &numPlatforms);

    cl_platform_id platform = platforms[0];

    cl_device_id devices[10];
    int numDevices;

    clGetDeviceIDs(platform,
            CL_DEVICE_TYPE_GPU,
            10,
            devices,
            &numDevices);

    cl_context context = clCreateContext(NULL,
            1,
            devices,
            NULL,
            NULL,
            NULL);

    const char* sourceCode = loadFromFile("kernel.cl");
    const size_t sourceCodeLength = strlen(sourceCode);
    cl_program program = clCreateProgramWithSource(context,
            1,
            &sourceCode,
            &sourceCodeLength,
            NULL);

    clBuildProgram(program,
            0,
            NULL,
            NULL,
            NULL,
            NULL);

    cl_kernel kernel = clCreateKernel(program, "matrixMultiplication", NULL);

    int rowsA = 4;
    int colsA = 3;
    int rowsB = 3;
    int colsB = 5;

    float matrixA[] = {
        1.f,  -1.f,  0.f,
        5.f,  10.f,  20.f,
        0.1f,  0.f,  3.f,
        0.5f, -0.1f, 100.f
    };

    float matrixB[] = {
        0.02f, 0.f,  50.f,  1.3f, 1.5f,
        1.f,   1.f,  2.f,   2.1f, 0.f,
        0.5f, -0.1f, 100.f, 0.9f, 1.1f
    };

    float matrixC[4*5];

    cl_mem matrixAOnGpu = clCreateBuffer(context,
            CL_MEM_READ_ONLY,
            sizeof(matrixA),
            NULL,
            NULL);

    cl_mem matrixBOnGpu = clCreateBuffer(context,
            CL_MEM_READ_ONLY,
            sizeof(matrixB),
            NULL,
            NULL);

    cl_mem matrixCOnGpu = clCreateBuffer(context,
            CL_MEM_WRITE_ONLY,
            sizeof(matrixC),
            NULL,
            NULL);

    clSetKernelArg(kernel, 0, sizeof(matrixAOnGpu), &matrixAOnGpu);
    clSetKernelArg(kernel, 1, sizeof(matrixBOnGpu), &matrixBOnGpu);
    clSetKernelArg(kernel, 2, sizeof(matrixCOnGpu), &matrixCOnGpu);
    clSetKernelArg(kernel, 3, sizeof(colsB), &colsB);
    clSetKernelArg(kernel, 4, sizeof(colsA), &colsA);

    cl_command_queue commandQueue = clCreateCommandQueueWithProperties(
            context,
            devices[0],
            0,
            NULL);

    clEnqueueWriteBuffer(commandQueue,
            matrixAOnGpu,
            CL_FALSE,
            0,
            sizeof(matrixA),
            matrixA,
            0,
            NULL,
            NULL);

    clEnqueueWriteBuffer(commandQueue,
            matrixBOnGpu,
            CL_FALSE,
            0,
            sizeof(matrixB),
            matrixB,
            0,
            NULL,
            NULL);

    size_t globalWorkSize[] = { rowsA, colsB };

    clEnqueueNDRangeKernel(commandQueue,
            kernel,
            2,
            NULL,
            globalWorkSize,
            NULL,
            0,
            NULL,
            NULL);

    clEnqueueReadBuffer(commandQueue,
            matrixCOnGpu,
            CL_TRUE,
            0,
            sizeof(matrixC),
            matrixC,
            0,
            NULL,
            NULL);

    printf("Resulting matrix C:\n");

    printf(" %f %f %f %f %f\n",
            matrixC[0], matrixC[1], matrixC[2], matrixC[3], matrixC[4]);
    printf(" %f %f %f %f %f\n",
            matrixC[5], matrixC[6], matrixC[7], matrixC[8], matrixC[9]);
    printf(" %f %f %f %f %f\n",
            matrixC[10], matrixC[11], matrixC[12], matrixC[13], matrixC[14]);
    printf(" %f %f %f %f %f\n",
            matrixC[15], matrixC[16], matrixC[17], matrixC[18], matrixC[19]);

    clReleaseCommandQueue(commandQueue);
    clReleaseMemObject(matrixCOnGpu);
    clReleaseMemObject(matrixBOnGpu);
    clReleaseMemObject(matrixAOnGpu);
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseContext(context);

    free((void*)sourceCode);

    return 0;
}
